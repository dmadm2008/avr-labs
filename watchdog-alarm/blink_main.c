#include <avr/io.h>
#include <util/delay.h>

#define LED_PIN PB0                     // PB0 as a LED pin
#define CTRL_PIN PB1

int
main(void)
{
    /* setup */
    // DDRB = 0b00000001;              // set LED pin as OUTPUT
    //DDRB != (1 << LED_PIN);
    //DDRB &= (1 << CTRL_PIN);
    DDRB = 0b00000001;
    PORTB = 0b00000000;
    // PORTB &= ~(1 << LED_PIN);
    PINB = 0b00000000;
    // PORTB &= ~(1 << CTRL_PIN);
    // PORTB = 0b00000000;             // set all pins to LOW

    /* loop */
    while (1) {
        PORTB ^= _BV(LED_PIN);  // toggle LED pin
        if ((PINB & (1 << CTRL_PIN)) > 0) {
            _delay_ms(1000);
        } else {
            _delay_ms(100);
        }
    }
}

