/*
 * GccApplication1.c
 *
 * Created: 3/25/2020 10:43:54 PM
 * Author : admin
 */
#include <util/delay.h>
#include <avr/cpufunc.h>
#include <avr/io.h>

#define SENSOR_LINE     PB3
#define RING_LINE       PB0
#define WAKEUP_LINE1    PB1
#define WAKEUP_LINE2    PB2
#define LED_PIN         PB4

#define pin_on(y)      PORTB |= (1 << y)
#define pin_off(x)     PORTB &= ~(1 << x)


void setup(void) {
	// outputs sets to 1
	DDRB |= (1 << RING_LINE) | \
	(1 << WAKEUP_LINE1) | \
	(1 << WAKEUP_LINE2) | \
	(1 << LED_PIN);
	DDRB &= ~(1 << SENSOR_LINE);

	_NOP();
	PORTB & ~(1 << SENSOR_LINE);
	_delay_ms(100);
}


// checking if the sensor has been triggered
int is_triggered(void) {
	int pos_checks = 0;
	int i;
	for (i=0; i < 20*5; i++) {
		if ((PINB & (1 << SENSOR_LINE)) > 0) {
			pos_checks += 1;
		}
		_delay_ms(10);
	}
	return (pos_checks >= 20*3) ? 1 : 0;
}


int main(void) {
	setup();
	while (1) {
		if (is_triggered() > 0) {
			pin_on(WAKEUP_LINE1);
			pin_on(WAKEUP_LINE2);
			_delay_ms(1000);
			pin_off(WAKEUP_LINE1);
			pin_off(WAKEUP_LINE2);
			pin_on(RING_LINE);
			_delay_ms(1000);
			pin_off(RING_LINE);
			_delay_ms(1000); // sleeping for 2 seconds
		} else {
		    _delay_ms(1000*2); // sleeping for 2 seconds
		}
		PORTB ^= _BV(LED_PIN);
	}

	return 0;
}
