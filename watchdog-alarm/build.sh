BIN=main.bin

test -f $BIN && rm $BIN

avr-gcc -Wall -g -Os -mmcu=attiny13 -o main.bin main.c

test -f $BIN || echo "**** FAILED ****"

avr-objcopy -j .text -j .data -O ihex main.bin main.hex

test -f main.hex && echo "**** SUCCESS ****"
